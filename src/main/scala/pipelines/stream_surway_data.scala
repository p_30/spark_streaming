package pipelines

import functions.{initialization, read, transform, write}

object stream_surway_data {

  def main(args: Array[String]): Unit = {

    val spark = initialization.sparkSession()

    val sc = initialization.sparkContext(spark)

    initialization.setLogLevel(sc, "ERROR")

    initialization.addFile(sc, variables.cassandra.connectBundlePath)

    val schema = transform.initSchema()

    var dataFrame = read.readKafka(spark, variables.kafka.bootstrapServers, "test_topic_2" )

    dataFrame = transform.transformJson(dataFrame, schema)

    write.writeStreamToCassandraCloud(dataFrame, "p_30", "surway_data", variables.cassandra.connectBundleFile, variables.cassandra.userName, variables.cassandra.password, "append")

  }
}

