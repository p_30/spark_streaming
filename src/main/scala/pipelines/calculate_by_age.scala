package pipelines

import functions.{initialization, read, transform, write}

object calculate_by_age {

  def main(args: Array[String]): Unit = {

    val spark = initialization.sparkSession()

    val sc = initialization.sparkContext(spark)

    initialization.setLogLevel(sc, "ERROR")

    initialization.addFile(sc, variables.cassandra.connectBundlePath)

    var dataFrame = read.readCassandraCloud(spark, "p_30", "surway_data", variables.cassandra.connectBundleFile, variables.cassandra.userName, variables.cassandra.password)

    dataFrame = transform.calculateByAge(dataFrame)

    dataFrame = transform.generateId(dataFrame)

    write.writeToCassandraCloud(dataFrame, "p_30", "surway_data_by_age", variables.cassandra.connectBundleFile, variables.cassandra.userName, variables.cassandra.password, "overwrite")

  }
}


