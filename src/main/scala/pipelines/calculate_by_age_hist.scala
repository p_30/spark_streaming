package pipelines

import functions.{initialization, read, transform, write}

object calculate_by_age_hist {
  def main(args: Array[String]): Unit = {

    val spark = initialization.sparkSession()

    val sc = initialization.sparkContext(spark)

    initialization.setLogLevel(sc, "ERROR")

    initialization.addFile(sc, variables.cassandra.connectBundlePath)

    var dataFrame = read.readCassandraCloud(spark, "p_30", "surway_data_by_age", variables.cassandra.connectBundleFile, variables.cassandra.userName, variables.cassandra.password)

    dataFrame = transform.renameId(dataFrame)

    dataFrame = transform.generateDate(dataFrame)
    
    write.writeToSqlServer(dataFrame, variables.sql_server.url, "dbo.SURWAY_DATA_BY_AGE_HIST", variables.sql_server.userName, variables.sql_server.password, "append")

  }
}
