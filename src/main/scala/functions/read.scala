package functions

import org.apache.spark.sql.{DataFrame, SparkSession}

object read {

  def readKafka(sparkSession: SparkSession, bootstrapServers: String, topics: String): DataFrame = {
    val dataFrame = sparkSession.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", bootstrapServers)
      .option("subscribe", topics)
      .load()

    dataFrame
  }

  def readCassandra(sparkSession: SparkSession, keySpace: String, table: String, host: String, port: String): DataFrame = {
    val dataFrame = sparkSession.read
      .format("org.apache.spark.sql.cassandra")
      .option("spark.cassandra.connection.host", host)
      .option("spark.cassandra.connection.port", port)
      .option("keyspace", keySpace)
      .option("table", table)
      .load()

    dataFrame
  }

  def readCassandraCloud(sparkSession: SparkSession, keySpace: String, table: String, connectBundle: String, userName: String, password: String): DataFrame = {
    val dataFrame = sparkSession.read
      .format("org.apache.spark.sql.cassandra")
      .option("spark.cassandra.connection.config.cloud.path", connectBundle)
      .option("spark.cassandra.auth.username", userName)
      .option("spark.cassandra.auth.password", password)
      .option("keyspace", keySpace)
      .option("table", table)
      .load()

    dataFrame
  }

}
