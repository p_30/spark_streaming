package functions

import org.apache.spark.sql.DataFrame

object write {
  def writeStreamToCassandraLocal(dataFrameIn: DataFrame, keySpace: String, table: String, host: String, port: String, mode: String): Unit = {
    dataFrameIn.writeStream
      .foreachBatch { (batchDF: DataFrame, batchId: Long) =>
        batchDF.write
          .format("org.apache.spark.sql.cassandra")
          .mode(mode)
          .option("spark.cassandra.connection.host", host)
          .option("spark.cassandra.connection.port", port)
          .option("keyspace", keySpace)
          .option("table", table)
          .save()
      }
      .outputMode(mode)
      .start()
      .awaitTermination()
  }

  def writeToCassandraLocal(dataFrameIn: DataFrame, keySpace: String, table: String, host: String, port: String, mode: String): Unit = {
    dataFrameIn.write
          .format("org.apache.spark.sql.cassandra")
          .mode(mode)
          .option("confirm.truncate", "true")
          .option("spark.cassandra.connection.host", host)
          .option("spark.cassandra.connection.port", port)
          .option("keyspace", keySpace)
          .option("table", table)
          .save()
  }

  def writeStreamToCassandraCloud(dataFrameIn: DataFrame, keySpace: String, table: String, connectBundle: String, userName: String, password: String, mode: String): Unit = {
    dataFrameIn.writeStream
      .foreachBatch { (batchDF: DataFrame, batchId: Long) =>
        batchDF.write
          .format("org.apache.spark.sql.cassandra")
          .option("spark.cassandra.connection.config.cloud.path", connectBundle)
          .option("spark.cassandra.auth.username", userName)
          .option("spark.cassandra.auth.password", password)
          .option("keyspace", keySpace)
          .option("table", table)
          .mode(mode)
          .save()
      }
      .outputMode(mode)
      .start()
      .awaitTermination()
  }

  def writeToCassandraCloud(dataFrameIn: DataFrame, keySpace: String, table: String, connectBundle: String, userName: String, password: String, mode: String): Unit = {
    dataFrameIn.write
      .format("org.apache.spark.sql.cassandra")
      .mode(mode)
      .option("confirm.truncate", "true")
      .option("spark.cassandra.connection.config.cloud.path", connectBundle)
      .option("spark.cassandra.auth.username", userName)
      .option("spark.cassandra.auth.password", password)
      .option("keyspace", keySpace)
      .option("table", table)
      .save()
  }

  def writeToSqlServer(dataFrameIn: DataFrame, url: String, table: String, userName: String, password: String, mode: String): Unit = {
    dataFrameIn.write
      .format("jdbc")
      .mode(mode)
      .option("url", url)
      .option("dbtable", table)
      .option("user", userName)
      .option("password", password)
      .option("driver", "com.microsoft.sqlserver.jdbc.SQLServerDriver")
      .save()
  }

}
