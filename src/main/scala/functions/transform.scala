package functions

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{IntegerType, StringType, StructType, TimestampType}

object transform {
  def initSchema(): StructType = {

    val schema = new StructType()
      .add("id",StringType)
      .add("name",StringType)
      .add("surname",StringType)
      .add("age",IntegerType)
      .add("date",TimestampType)
      .add("answer_1",StringType)
      .add("answer_2",StringType)
      .add("answer_3",StringType)
      .add("amount",IntegerType)
      .add("rating",IntegerType)

    schema;
  }

  def transformJson(dataFrameIn: DataFrame, schema: StructType): DataFrame = {
    val dataFrameOut = dataFrameIn.selectExpr("CAST(value AS STRING)")
      .select(from_json(col("value"), schema).as("data"))
      .select("data.*")

    dataFrameOut
  }

  def calculateByAge(dataFrameIn: DataFrame): DataFrame = {
        val dataFrameOut =  dataFrameIn.groupBy("age").agg(
          countDistinct("rating").as("count_rating"),
          avg("rating").as("avg_rating"),
          min("rating").as("min_rating"),
          max("rating").as("max_rating"),
          countDistinct("amount").as("count_amount"),
          avg("amount").as("avg_amount"),
          min("amount").as("min_amount"),
          max("amount").as("max_amount")
        )

    dataFrameOut
  }

  def generateId(dataFrameIn: DataFrame): DataFrame = {
    val dataFrameOut = dataFrameIn.withColumn("id", expr("uuid()"))

    dataFrameOut
  }

  def generateDate(dataFrameIn: DataFrame): DataFrame = {
    val dataFrameOut = dataFrameIn.withColumn("date", expr("current_timestamp()"))

    dataFrameOut
  }

  def renameId(dataFrameIn: DataFrame): DataFrame = {
    val dataFrameOut = dataFrameIn.withColumnRenamed("id","id_uuid")

    dataFrameOut
  }

}
