package functions

import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession

object initialization {

  def sparkSession(): SparkSession = {
       val spark = SparkSession
          .builder()
          .master("local[*]")
          .appName("SparkStreaming")
          .getOrCreate()

       spark
  }

  def sparkContext(sparkSession: SparkSession): SparkContext = {

    val sc = sparkSession.sparkContext

    sc;
  }

  def setLogLevel(sparkContext: SparkContext, logLevel: String): Unit = {

    sparkContext.setLogLevel(logLevel)

  }

  def addFile(sparkContext: SparkContext, path: String): Unit = {

    sparkContext.addFile(path)

  }
}
